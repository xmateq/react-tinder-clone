import React from 'react';
import './Chats.css';
import Chat from './Chat';

function Chats () {
  return (
    <div className="chats">
      <Chat 
        name="Mark"
        message="Yo, what's up?"
        timestamp="40 seconds ago"
        profilePic="..."
      />
      <Chat 
        name="Rafeh"
        message="Heyyy"
        timestamp="45 minutes ago"
        profilePic="https://images.chesscomfiles.com/uploads/v1/user/1729189.eef27988.1200x1200o.b224ab4fa4a0.jpeg"
      />
      <Chat 
        name="Sonny"
        message="Yoooo"
        timestamp="2 days ago"
        profilePic="https://avatars.githubusercontent.com/u/24712956?s=400&u=b71527e605ae1b748fc2d4157a842e57e427ad44&v=4"
      />
      <Chat 
        name="Elen"
        message="Hi there :)"
        timestamp="1 week ago"
        profilePic="..."
      />
    </div>
  )
}

export default Chats;