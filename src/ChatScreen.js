import React, { useState } from 'react';
import './ChatScreen.css';
import Avatar from '@material-ui/core/Avatar';

function ChatScreen () {

  const [input, setInput] = useState("");
  const [messages, setMessages] = useState([
    {
      name: "Rafeh",
      image: "https://images.chesscomfiles.com/uploads/v1/user/1729189.eef27988.1200x1200o.b224ab4fa4a0.jpeg",
      message: "Hello.."
    },
    {
      name: "Sonny",
      image: "https://avatars.githubusercontent.com/u/24712956?s=400&u=b71527e605ae1b748fc2d4157a842e57e427ad44&v=4",
      message: "Yoooo",
    },
    {
      message: "It's okay"
    }
  ]);

  const handleSend = (e) => {
    e.preventDefault();

    setMessages([...messages, { message: input }]);
    setInput("");
  }

  return (
    <div className="chatScreen">
      <p className="chatScreen__timestamp">YOU MATCHED WITH RAFEH ON 11/07/2020</p>
      {messages.map(message =>(
        message.name ? (
          <div className="chatScreen__message">
            <Avatar
              className="chatScreen__image"
              src={message.image}
              alt={message.name}
            />
            <p className="chatScreen__text">{message.message}</p>
          </div>
        ) : (
          <div className="chatScreen__message">
            <p className="chatScreen__textUser">{message.message}</p>
          </div>
        )
        
      ))}
      <form className="chatScreen__input">
        <input 
          value={input}
          onChange={e => setInput(e.target.value)}
          className="chatScreen__inputField" 
          placeholder="Type a message.." 
          type="text"
        />
        <button 
          onClick={handleSend} 
          type="submit" 
          className="chatScreen__inputButton">SEND
        </button>
      </form>
    </div>
  )
}

export default ChatScreen;