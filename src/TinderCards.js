import React, { useState } from 'react';
import TinderCard from 'react-tinder-card';
import './TinderCards.css';

function TinderCards () {

  const [people, setPeople] = useState([
    {
      name: "Rafeh Qazi",
      url: 'https://images.chesscomfiles.com/uploads/v1/user/1729189.eef27988.1200x1200o.b224ab4fa4a0.jpeg'
    },
    {
      name: "Sonny Sangha",
      url: 'https://avatars.githubusercontent.com/u/24712956?s=400&u=b71527e605ae1b748fc2d4157a842e57e427ad44&v=4'
    }
  ]);

  return (
    <div className="tinderCards">
      <div className="tinderCards__cardContainer">
        {
          people.map(person => (
            <TinderCard
              className="tinderCards__swipe"
              key={person.name}
              preventSwipe={['up', 'down']}
              // always use a key !!
            >
              <div 
                style={{ backgroundImage: `url(${person.url})` }}
                className="tinderCards__card">
                <h3>{person.name}</h3>
              </div>
            </TinderCard>
          ))
        }
      </div>
    </div>
  )
}

export default TinderCards;